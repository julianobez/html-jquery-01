# HTML + CSS + JQuery + Git - Desafio 01 #

Desafio para colocar em prática alguns conhecimentos adquiridos e aprender um pouco mais sobre o que está por vir.

### Instruções ###

* Faça o clone desse repositório git em seu workspace
* Inicialmente ele estará vazio
* Clonou o repositório? Então agora vamos codar!

### Tarefa ###

* Crie uma página html;
* Trata-se de uma página que representará uma parede de uma galeria de arte que possui 1 quadro ali pendurado
* Ela deve ter uma div centralizada, que representará a moldura da obra de arte;
* Abaixo dessa div, teremos alguns botões html os quais serão responsáveis por executar algumas ações;
* o primeiro botão deverá trocar a imagem da moldura. A cada clique as imagens devem se alternar. Podemos trabalhar inicialmente com 2 imagens apenas
* o segundo botão, irá dar zoom in na imagem a cada clique
* o terceiro botão, irá dar zoom out na imagem a cada clique
* o quarto botão irá devolver a imagem ao seu tamanho inicial
* O quinto botão irá mudar a cor de fundo da 'parede' a cada clique, através de css inline
* o sexto botão irá mudar a cor de fundo da 'parede' a cada clique, através de mudança de classe css

### Sugestões ###

* Use jquery para implementar os comportamentos
* Implemente o comportamento em um arquivo javascript separado do HTML
* Implemente o css em um arquivo separado do html
* A cada pequena etapa/checkpoint, fala um commit do seu código, detalhando na mensagem o que foi feito naquele commit e faça um push do código ao repositório.
* Ao final de cada dia, antes de parar, faça um commit do seu código onde parou e um push ao repositório.
* Teve dúvida? pesquise em inglês e priorize respostas do stack overflow

### O que se espera ###

* O prazo para entrega é terça, 29/09.
* A ideia do desafio é justamente começar a quebrar a barreira da dependência do conhecimento prévio, forçar o auto aprendizado e a pesquisa pontual para resolver problemas pontuais.
