var Galeria = function(){};

$(function () {
    Galeria.init();
});

Galeria.init = function(){
    Galeria.aplicarAcaoTrocaImagem();
    Galeria.aplicarAcaoZoomIn();
    Galeria.aplicarAcaoZoomOut();
    Galeria.aplicarAcaoZoomConfiguracoesIniciais();
    Galeria.aplicarAcaoTrocaBackgroundPorCssInline();
    Galeria.aplicarAcaoTrocaBackgroundPorClasseCss();
}

Galeria.aplicarAcaoTrocaImagem = function(){
    let indexAtual = 0;
    let imagens = [];
    imagens.push({caminhoImagem:'_img/monalisa.jpg'});
    imagens.push({caminhoImagem:'_img/monalisa2.jpg'});
    imagens.push({caminhoImagem:'_img/arte3.jpg'});
    imagens.push({caminhoImagem:'_img/arte4.jpg'});

    $("#button-1").click(function () {
        indexAtual++;
        if(indexAtual >= imagens.length){
            indexAtual = 0;
        }
        $('.main_galery_content_img img').attr('src', imagens[indexAtual].caminhoImagem);

    });
};

var currentZoom = 1.0;
Galeria.aplicarAcaoZoomIn = function(){
    $('#button-2').click(
        function () {
            Galeria.aplicaZoomGaleria(false);
        });
};

Galeria.aplicarAcaoZoomOut = function(){
    $('#button-3').click(
        function () {
            Galeria.aplicaZoomGaleria(true);
        });
};

Galeria.aplicaZoomGaleria = function(zoomOut){
    currentZoom = (zoomOut === true) ? (currentZoom - .1) : (currentZoom + .1);
    $('.main_galery_content_img img').animate({'zoom': currentZoom}, 'slow');
}


Galeria.aplicarAcaoZoomConfiguracoesIniciais = function(){
    $('#button-4').click(
        function () {
            currentZoom = 1.0
            $('.main_galery_content_img img').animate({'zoom': 1}, 'slow');
            $('.main_galery').css({"background-color": "#2D3142"});
            $('.main_galery').removeClass('background_button_6');
        });
};

Galeria.aplicarAcaoTrocaBackgroundPorCssInline = function(){
    $('#button-5').click(function () {
        $('.main_galery').each(function (index) {
            var colorR = Math.floor((Math.random() * 256));
            var colorG = Math.floor((Math.random() * 256));
            var colorB = Math.floor((Math.random() * 256));
            $(this).css("background-color", "rgb(" + colorR + "," + colorG + "," + colorB + ")");
        });
    });
}

Galeria.aplicarAcaoTrocaBackgroundPorClasseCss = function(){
    $('#button-6').click(function () {
        $('#main_galery').toggleClass('background_button_6');
    });
}
    
//$(function () {
//    $('.button-2').on('click', function () {
//            var img = $(this).find('.img');
//            var width = img.width();
//            var newWidth = width + 100;
//            img.width(newWidth);
//        }
//    );
//    $('.button-3').on('click', function () {
//            var img = $(this).find('.main_galery_content_img img');
//            var width = img.width();
//            var newWidth = width - 100;
//            img.width(newWidth);
//        }
//    );
//});


//$(function () {
//$('.button-2').click(function()
//     {$('.main_galery_content_img img').addClass('zoom')});
//});


//$(function () {
//    $('.button-3').click(function()
//    {$('.main_galery_content_img img').addClass('zoom_out')});
//  });

//$(function () {
//  $('.button-4').click(function()
//    {$('.main_galery_content_img img').removeClass('zoom zoom_out')});
//  });

//$(function () {
//    $('.button-5').click(function() {
//       var test = false;
//        if (test == false) {
//            var test = true;
//            $('.main_galery').css({"background-color": "#FFF"});
//            $('.main_galery_content').css({"color": "#2D3142"});
//            $('.main_galery_content_img img').css({"background-color": "#2D3142"});
//            $('.main_galery_button').css({"background-color": "#CCC"});
//  }
//        else if(test == true) {
//            $('.main_galery').css({"background-color": "red"});
//            $('.main_galery_content').css({"color": "red"});
//            $('.main_galery_content_img img').css({"background-color": "blue"});
//            $('.main_galery_button').css({"background-color": "yellow"});
//        var test = false;
//    }
//    });
//  });


//$(document).ready(function () {
//    var back = ["#ff0000","blue","gray"];
//    var rand = back[Math.floor(Math.random() * back.length)];
//
//    $('.button-5').click(function () {
//    $('.main_galery').css('background',rand);
//        });
//});