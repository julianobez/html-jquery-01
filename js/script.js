class Galeria {


    constructor(){
        this.currentZoom = 1.0;

        $(() => {
            this.init();
        });
    }
    
    init(){
        this.aplicarAcaoTrocaImagem();
        this.aplicarAcaoZoomIn();
        this.aplicarAcaoZoomOut();
        this.aplicarAcaoZoomConfiguracoesIniciais();
        this.aplicarAcaoTrocaBackgroundPorCssInline();
        this.aplicarAcaoTrocaBackgroundPorClasseCss();
    }
    
    aplicarAcaoTrocaImagem(){
        let indexAtual = 0;
        let imagens = [];
        imagens.push({caminhoImagem:'_img/monalisa.jpg'});
        imagens.push({caminhoImagem:'_img/monalisa2.jpg'});
        imagens.push({caminhoImagem:'_img/arte3.jpg'});
        imagens.push({caminhoImagem:'_img/arte4.jpg'});
    
        $("#button-1").click(function () {
            indexAtual++;
            if(indexAtual >= imagens.length){
                indexAtual = 0;
            }
            $('.main_galery_content_img img').attr('src', imagens[indexAtual].caminhoImagem);
    
        });
    };
    
    aplicarAcaoZoomIn (){
        let _self = this;
        $('#button-2').click(
            function () {
                _self.aplicaZoomGaleria(false);
            });
    };
    
    aplicarAcaoZoomOut (){
        let _self = this;
        $('#button-3').click(
            function () {
                _self.aplicaZoomGaleria(true);
            });
    };
    
    aplicaZoomGaleria(zoomOut){
        this.currentZoom = (zoomOut === true) ? (this.currentZoom - .1) : (this.currentZoom + .1);
        $('.main_galery_content_img img').animate({'zoom': this.currentZoom}, 'slow');
    }
    
    
    aplicarAcaoZoomConfiguracoesIniciais(){
        let _self = this;
        $('#button-4').click(
            function () {
                _self.currentZoom = 1.0
                $('.main_galery_content_img img').animate({'zoom': 1}, 'slow');
                $('.main_galery').css({"background-color": "#2D3142"});
                $('.main_galery').removeClass('background_button_6');
            });
    };
    
    aplicarAcaoTrocaBackgroundPorCssInline(){
        $('#button-5').click(function () {
            $('.main_galery').each(function (index) {
                var colorR = Math.floor((Math.random() * 256));
                var colorG = Math.floor((Math.random() * 256));
                var colorB = Math.floor((Math.random() * 256));
                $(this).css("background-color", "rgb(" + colorR + "," + colorG + "," + colorB + ")");
            });
        });
    }
    
    aplicarAcaoTrocaBackgroundPorClasseCss(){
        $('#button-6').click(function () {
            $('#main_galery').css("background-color", "");
            $('#main_galery').toggleClass('background_button_6');
        });
    }

}
new Galeria();
